from random import random
import plotly.figure_factory as ff
import numpy as np


# https://plot.ly/python/gantt/
# https://community.plot.ly/t/plotly-colours-list/11730/2


class Node:

    def __init__(self, time):
        self.id = 0

        self.nodes_before = []
        self.nodes_after = []

        self.time = time
        self.start_time = 0
        self.early_end_time = 0
        self.late_end_time = 0

    def calculate_early_end_time(self):
        if self.nodes_before:
            self.early_end_time = max(node.early_end_time for node in self.nodes_before) + self.time
        else:
            self.early_end_time = self.time

    def calculate_start_time(self):
        self.start_time = self.early_end_time - self.time

    def calculate_late_end_time(self):
        if self.nodes_after:
            max_value = 0
            found_node = None
            for node in self.nodes_after:
                if max_value <= node.early_end_time:
                    found_node = node
            self.late_end_time = found_node.early_end_time - found_node.time
        else:
            self.late_end_time = self.early_end_time

    def __repr__(self):
        return "z" + str(self.id) + \
               "(T:" + str(self.time) + \
               ", S:" + str(self.start_time) + \
               ", E:" + str(self.early_end_time) + \
               ", L:" + str(self.late_end_time) + \
               ")"


class Machine:

    def __init__(self, id):
        self.id = id
        self.tasks = []
        self.last_task_early_end_time = 0

    def add_task(self, task):
        self.tasks.append(task)

    def update_last_task_early_end_time(self):
        self.last_task_early_end_time = self.tasks[len(self.tasks) - 1].early_end_time

    def __repr__(self):
        return "m" + str(self.id) + " " + str(self.tasks)


class Network:

    def __init__(self, nodes):
        self.nodes = nodes
        self.machines = []

        self.indexer = 1
        for node in self.nodes:
            node.id = self.indexer
            self.indexer += 1
        self.indexer = 1

    def connect(self, id_one, id_two):
        if id_one >= id_two:
            raise Exception("Bad topology")

        node_one = self.nodes[id_one - 1]
        node_two = self.nodes[id_two - 1]

        node_one.nodes_after.append(node_two)
        node_two.nodes_before.append(node_one)

    def create_plot(self):
        self.fill_node_values()
        self.fill_machines()
        self.show_plot()

    def fill_node_values(self):
        for node in self.nodes:
            node.calculate_early_end_time()
        for node in self.nodes:
            node.calculate_start_time()
        for node in self.nodes:
            node.calculate_late_end_time()

    def fill_machines(self):
        for task in self.nodes:
            machine = self.__find_available_machine(task)
            machine.add_task(task)
            machine.update_last_task_early_end_time()

    def __find_available_machine(self, task):
        for machine in self.machines:
            if task.start_time >= machine.last_task_early_end_time:
                return machine
        self.__add_new_machine()
        return self.machines[len(self.machines) - 1]

    def __add_new_machine(self):
        self.machines.append(Machine(self.indexer))
        self.indexer += 1

    def show_plot(self):
        plot_dict = []
        colors = []
        rand = lambda: int(random() * 255)

        self.machines.reverse()
        for machine in self.machines:
            for task in machine.tasks:
                plot_dict.append({
                    'Task': str(machine.id),
                    'Start': str(task.start_time),
                    'Finish': str(task.early_end_time),
                    'Id': "I:{} E:{} L:{}".format(str(task.id), task.early_end_time, task.late_end_time)
                })
                colors.append('#%02X%02X%02X' % (rand(), rand(), rand()))

        fig = ff.create_gantt(plot_dict, colors=colors, group_tasks=True, index_col='Id', task_names='Id')
        fig.update_yaxes(title_text='Machine')
        fig.update_xaxes(title_text='Time')
        fig['layout']['xaxis']['tickvals'] = np.arange(0, max([node.early_end_time for node in self.nodes]))
        fig.layout.xaxis.type = 'linear'
        fig.show()


def create_network():
    z1 = Node(3)
    z2 = Node(8)
    z3 = Node(2)
    z4 = Node(2)
    z5 = Node(4)
    z6 = Node(6)
    z7 = Node(9)
    z8 = Node(2)
    z9 = Node(1)
    z10 = Node(2)
    z11 = Node(1)
    z12 = Node(2)
    z13 = Node(6)
    z14 = Node(5)
    z15 = Node(9)
    z16 = Node(6)
    z17 = Node(2)
    z18 = Node(5)
    z19 = Node(3)

    node_list = [z1, z2, z3, z4, z5, z6, z7, z8, z9, z10,
                 z11, z12, z13, z14, z15, z16, z17, z18, z19]

    network = Network(node_list)

    network.connect(1, 4)
    network.connect(1, 5)
    network.connect(3, 6)
    network.connect(3, 7)
    network.connect(4, 8)
    network.connect(5, 9)
    network.connect(5, 10)
    network.connect(2, 9)
    network.connect(2, 10)
    network.connect(6, 10)
    network.connect(7, 11)
    network.connect(7, 12)
    network.connect(8, 13)
    network.connect(9, 13)
    network.connect(10, 14)
    network.connect(10, 15)
    network.connect(10, 16)
    network.connect(11, 14)
    network.connect(11, 15)
    network.connect(11, 16)
    network.connect(12, 17)
    network.connect(13, 18)
    network.connect(14, 18)
    network.connect(16, 19)
    network.connect(17, 19)

    network.create_plot()


create_network()
