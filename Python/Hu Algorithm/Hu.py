from random import random

import plotly.figure_factory as ff
import numpy as np


class Node:

    def __init__(self, id, time):
        self.id = id
        self.time = time
        self.start_time = 0
        self.in_progress = False

        self.nodes_before = []
        self.nodes_after = []

    def remove_connections(self):
        for node in self.nodes_after:
            if node.nodes_before:
                node.nodes_before.remove(self)


class Machine:

    def __init__(self, id):
        self.id = id
        self.nodes = []
        self.current_time = 0

    def add_node(self, node):
        self.nodes.append(node)
        node.remove_connections()

        node.in_progress = True
        node.start_time = self.current_time
        self.current_time += node.time


class Network:

    def __init__(self):
        self.nodes = []
        self.machines = []

        self.__get_input_values()

    def __get_input_values(self):
        print("Number of machines: ", end="")
        for i in range(0, int(input())):
            self.machines.append(Machine(i + 1))

        print("Task duration: ", end="")
        time = int(input())

        print("Number of tasks: ", end="")
        for i in range(0, int(input())):
            self.nodes.append(Node(i + 1, time))

    def connect(self, id_one, id_two):
        if id_one >= id_two:
            raise Exception("Bad topology")

        node_one = self.nodes[id_one - 1]
        node_two = self.nodes[id_two - 1]

        node_one.nodes_after.append(node_two)
        node_two.nodes_before.append(node_one)

    def create_plot(self):
        self.__fill_machines()
        self.__show_plot()

    def __fill_machines(self):
        while True:
            indexer = 0
            ready_nodes = self.__find_ready_nodes()
            if not ready_nodes:
                break
            for node in ready_nodes:
                if indexer >= len(self.machines):
                    indexer = 0

                machine = self.machines[indexer]
                machine.add_node(node)
                indexer += 1

    def __find_ready_nodes(self):
        return [node for node in self.nodes if not node.nodes_before and not node.in_progress]

    def __show_plot(self):
        plot_dict = []
        colors = []
        rand = lambda: int(random() * 255)

        for machine in self.machines:
            for node in machine.nodes:
                plot_dict.append({
                    'Task': str(machine.id),
                    'Start': str(node.start_time),
                    'Finish': str(node.start_time + node.time),
                    'Id': "Id:{}".format(str(node.id))
                })
                colors.append('#%02X%02X%02X' % (rand(), rand(), rand()))

        fig = ff.create_gantt(plot_dict, colors=colors, group_tasks=True, index_col='Id', task_names='Id')
        fig.update_yaxes(title_text='Machine')
        fig.update_xaxes(title_text='Time')
        fig.layout.xaxis.type = 'linear'
        fig.show()


def create_network():
    network = Network()

    network.connect(1, 7)
    network.connect(2, 7)
    network.connect(3, 8)
    network.connect(4, 9)
    network.connect(5, 9)
    network.connect(6, 10)
    network.connect(7, 10)
    network.connect(8, 11)
    network.connect(9, 11)
    network.connect(10, 12)
    network.connect(11, 12)

    network.create_plot()


create_network()
