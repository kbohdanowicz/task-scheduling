import plotly.figure_factory as ff
# https://plot.ly/python/gantt/
# https://community.plot.ly/t/plotly-colours-list/11730/2


class Task:
    indexer = 1

    def __init__(self, time, id=None):
        self.time = time
        self.start_time = 0
        if id is None:
            self.id = Task.indexer
            Task.indexer += 1
        else:
            self.id = id

    def split(self, time_remaining):
        split_time = self.time - time_remaining
        self.time = time_remaining
        return Task(split_time, self.id)

    def __repr__(self):
        return "t" + str(self.id) + ": " + str(int(self.time)) + \
               " (" + str(int(self.start_time)) + " " + str(int(self.start_time + self.time)) + ")"


class Machine:
    indexer = 1

    def __init__(self, id=None):
        self.tasks = []
        self.time_remaining = None
        if id is None:
            self.id = Machine.indexer
            Machine.indexer += 1
        else:
            self.id = id

    def add_task(self, task):
        if len(self.tasks) > 0:
            last_task = self.tasks[len(self.tasks) - 1]
            task.start_time = last_task.start_time + last_task.time
        self.tasks.append(task)

    def __repr__(self):
        return "m" + str(self.id) + " " + str(self.tasks)


class McNaughton:

    def __init__(self):
        self.tasks = []
        self.machines = []

        self.get_input()

        self.optimal_time = self.get_optimal_time()

        print("\nCmax* = " + str(int(self.optimal_time)))
        print("========================================================")

        split_task = None
        for machine in self.machines:
            machine.time_remaining = self.optimal_time

            while len(self.tasks) > 0:
                if machine.time_remaining - self.tasks[0].time < 0:
                    split_task = self.tasks[0].split(machine.time_remaining)
                    machine.add_task(self.tasks[0])
                    break

                if split_task is not None:
                    machine.add_task(split_task)
                    machine.time_remaining -= split_task.time
                    split_task = None
                else:
                    machine.add_task(self.tasks[0])
                    machine.time_remaining -= self.tasks[0].time
                self.tasks.remove(self.tasks[0])
            print(machine)

    def get_input(self):
        print("Number of machines: ", end="")
        for i in range(0, int(input())):
            self.machines.append(Machine())

        print("Number of tasks: ", end="")
        tasks_count = input()

        print("Task durations:")
        for i in range(0, int(tasks_count)):
            print("Task " + str(i + 1) + ": ", end="")
            self.tasks.append(Task(int(input())))

    def get_optimal_time(self):
        _sum = sum([task.time for task in self.tasks])
        _max = max([task.time for task in self.tasks])
        return max(_sum / len(self.machines), _max)

    def print_plot(self):
        plot_dictionary = []
        for machine in self.machines:
            for task in machine.tasks:
                plot_dictionary.append({
                    'Task': str(machine.id),
                    'Start': str(task.start_time),
                    'Finish': str(task.start_time + task.time),
                    'Id': "Task " + str(task.id)
                })

        fig = ff.create_gantt(plot_dictionary, group_tasks=True, index_col='Id', task_names='Id')
        fig.update_yaxes(title_text='Machine')
        fig.update_xaxes(title_text='Time')
        fig.layout.xaxis.type = 'linear'
        fig.show()


mcnaughton = McNaughton()
mcnaughton.print_plot()
