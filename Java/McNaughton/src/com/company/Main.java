package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringJoiner;

public class Main {

    public static int getOptimalTime(int machineCount, List<Integer> originalTasks){
        int sum = 0;
        for (Integer taskDuration : originalTasks) {
            sum += taskDuration;
        }

        int maxValue = 0;
        for (Integer taskDuration : originalTasks) {
            if (maxValue < taskDuration)
                maxValue = taskDuration;
        }

        return Math.max(sum / machineCount, maxValue);
    }

    public static void printResults(int machineCount, List<Integer> originalTasks) {
        int timeRemaining;
        int surplusTime = 0;
        int taskIndex = 0;
        List<Integer> tasks = new ArrayList<>(originalTasks);

        System.out.println("=========================================================================================");
        for (int i = 0; i < machineCount; i++) {
            StringJoiner joiner = new StringJoiner(", ","[","]");
            timeRemaining = getOptimalTime(machineCount, originalTasks);

            System.out.print("m" + (i + 1) + " ");
            while (!tasks.isEmpty()) {
                if (timeRemaining - tasks.get(0) < 0) {
                    taskIndex++;
                    if (timeRemaining != 0)
                        joiner.add("z" + taskIndex + ": " + timeRemaining);
                    surplusTime = tasks.get(0) - timeRemaining;
                    break;
                }

                if (surplusTime != 0){
                    joiner.add("z" + taskIndex + ": " + surplusTime);
                    timeRemaining -= surplusTime;
                    surplusTime = 0;
                }
                else {
                    taskIndex++;
                    joiner.add("z" + taskIndex + ": " + tasks.get(0).toString());
                    timeRemaining -= tasks.get(0);
                }
                tasks.remove(0);
            }
            System.out.print(joiner + "\n");
        }
    }

    public static void printPlot(int machineCount, List<Integer> originalTasks) {
        int timeRemaining;
        int surplusTime = 0;
        int taskIndex = 0;
        List<Integer> tasks = new ArrayList<>(originalTasks);

        System.out.println("=========================================================================================");
        for (int i = 0; i < machineCount; i++) {
            StringJoiner joiner = new StringJoiner(" ","[","]");
            timeRemaining = getOptimalTime(machineCount, originalTasks);

            System.out.print("m" + (i + 1) + " ");
            while (!tasks.isEmpty()) {
                if (timeRemaining - tasks.get(0) < 0) {
                    taskIndex++;
                    if (timeRemaining != 0)
                        joiner.add(repeatSymbolString(String.valueOf(taskIndex), timeRemaining,
                                originalTasks.get(originalTasks.size() - 1))
                        );
                    surplusTime = tasks.get(0) - timeRemaining;
                    break;
                }

                if (surplusTime != 0){
                    joiner.add(repeatSymbolString(String.valueOf(taskIndex), surplusTime,
                            originalTasks.get(originalTasks.size() - 1))
                    );
                    timeRemaining -= surplusTime;
                    surplusTime = 0;
                }
                else {
                    taskIndex++;
                    joiner.add(repeatSymbolString(String.valueOf(taskIndex), tasks.get(0),
                            originalTasks.get(originalTasks.size() - 1))
                    );
                    timeRemaining -= tasks.get(0);
                }
                tasks.remove(0);
            }
            System.out.print(joiner + "\n");
        }
    }

    private static String repeatSymbolString(String symbol, int quantity, int length){
        StringJoiner str = new StringJoiner(" ");
        for (int i = 0; i < quantity; i++) {
            str.add(fixedLengthString(symbol, length));
        }
        return str.toString();
    }

    public static String fixedLengthString(String string, int length) {
        return String.format("%1$" + length + "s", string);
    }

    public static void main(String[] args) {

        int machineCount;
        int taskCount;
        List<Integer> originalTasks = new ArrayList<>();

        Scanner sc = new Scanner(System.in);
        System.out.print("Number of machines: ");
        machineCount = sc.nextInt();

        System.out.print("Number of tasks: ");
        taskCount = sc.nextInt();

        System.out.println("Task durations: ");
        for (int i = 0; i < taskCount; i++) {
            System.out.print("z" + (i + 1) + ": ");
            originalTasks.add(sc.nextInt());
        }

        System.out.println("C_max* = " + getOptimalTime(machineCount, originalTasks));

        printResults(machineCount, originalTasks);
        printPlot(machineCount, originalTasks);
    }
}
